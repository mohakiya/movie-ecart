# Movie eCart Web app

In this movie-ecart web app, I display the movie item as the descending order of popularity and added the task’s function.

## Getting Started with Movie eCart Web app

If you are new to run react project, you’ll need to have Node.js and the Node Package Manager(npm) installed on your machine.
Visit to the official Node.js website(https://nodejs.org/) and download and install the latest version of Node.js for your operating system.

### first step : `npm start`

When you run with `npm start`, you got below error in terminal/command line :

Error: error: 0308010C:digital envelope routines: : unsupported
at new Hash (node: internal/crypto/hash:71: 19)
at Object. createHash (node: crypto: 133: 10)
at module. exports 135: 53)
at NormalModule.\_initBuildHash js:417: 16)
at handleParseError js:471: 10)
at /user/local/lib/node_modules/react-scripts/node_modules/webpack/lib/NormalModule.js:503:5
at /user/local/lib/node_modules/react-scripts/node_modules/webpack/lib/NormalModule.js:358:12
at /user/local/lib/node_modules/react-scripts/node_modules/loader-runner/lib/LoaderRunner.js:373:3
at iterateNormalLoaders 10)
at iterateNormalLoaders 10)
at /user/local/lib/node_modules/react-scripts/scripts/start.js:19 throw err;

Error: error: 0308010C:digital envelope routines: : unsupported
new Hash (node: internal/crypto/hash:71:19)
at Object.createHash (node: crypto:133:10)
at module.exports 135:53)
at NormalModule.\_initBuildHash 16)
at /user/local/lib/node_modules/react-scripts/node_modules/webpack/lib/NormalModule.js:452:10
at /user/local/lib/node_modules/react-scripts/node_modules/webpack/lib/NormalModule.js:323:13
at /user/local/lib/node_modules/react-scripts/node_modules/loader-runner/lib/LoaderRunner.js:367:11
at /user/local/lib/node_modules/ react-sc ripts/node_modules/loade r- runne r/lib/Loade rRunner.js:233:18
context.callback(/user/local/lib/node_modules/react-scripts/node_modules/loader-runner/lib/LoaderRunner.js:111:13)
at /user/local/lib/node_modules/react-scripts/node_modules/babel-loader/lib/index.js:59:103 {
opensslErrorStack: [ envelope routines: : initialization error' ] ,
library: 'digital envelope routines ,
reason : 'unsupported' ,
code: 'ERR OSSL EVP UNSUPPORTED'
}
Node.js v18.9.0

So please fix this error by `nvm install 14.16.0`.

## I used the following 5 libraries in this app. If you need to install libray please us within parentheses :

### @reah/router(`npm i @reach/router`)

### react-bootstrap(`npm i react-bootsrap`)

### react-icons(`npm i react-icons`)

### react-use-cart(`npm i react-use-cart`)

### react-filter-search(`npm i react-filter-search`)
