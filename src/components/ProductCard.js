import React from "react";
import { useCart } from "react-use-cart";
import { BsCartPlus } from "react-icons/bs";
import { FiStar } from "react-icons/fi";
import "./css/style.css";

const ProductCard = (props) => {
  let img_path = "https://image.tmdb.org/t/p/w500"; //image address to get the right image

  let { poster_path, title, vote_average, overview, release_date } = props.data; //passing data from parent

  const { addItem } = useCart(); //shopping cart feature library

  //adding to cart
  const addToCart = () => {
    addItem({ ...props.data, price: 0 }); // In useCart library, if there is no price value it does not allow to pass data all. Price object does not exit in api response so it place as 0
  };
  return (
    <div className="movie">
      <div style={{ float: "right" }}>
        <BsCartPlus
          size="1.8rem"
          className="addtobasket"
          onClick={() => addToCart()}
        />
      </div>
      <img src={img_path + poster_path} className="poster" alt="movie" />
      <div className="movie-details">
        <div className="box">
          <h4 className="title" style={{ float: "left", color: "#22205d" }}>
            {title}
          </h4>

          <p className="rating" style={{ float: "right", color: "#22205d" }}>
            <FiStar size="1.4rem" />
            {vote_average}
          </p>
        </div>

        <div className="overview">
          <h6>
            {title}:<span>{release_date}</span>
          </h6>
          {overview}
        </div>
      </div>
    </div>
  );
};

export default ProductCard;
