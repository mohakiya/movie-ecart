import React, { useEffect, useState } from "react";
import { Container, Row, Col, InputGroup, FormControl } from "react-bootstrap";
import { BiSearch } from "react-icons/bi";
import SearchFilter from "react-filter-search";
import ProductCard from "../ProductCard";
import "../css/style.css";
import _ from "lodash";

const Home = () => {
  const [searchMovie, setSearchMovie] = useState(""); //state for search movie name
  const [movieData, setMovieData] = useState([]); //state for movie item

  //api call function by using fetch method
  const getResponse = async () => {
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch(
      "https://api.themoviedb.org/3/search/movie?api_key=a5e48fc684e9278a4b85ffec1d020b86&query=a",
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => {
        setMovieData(_.orderBy(result.results, ["popularity"], ["desc"]));
      })
      .catch((error) => console.log("error", error));
  };

  //to display the data as soon as the page is opening
  useEffect(() => {
    getResponse();
  }, []);

  return (
    <Container className="py-4">
      <Row className="justify-content-center">
        <Col xs={10} md={7} lg={6} xl={4} className="mb-3 mx-auto text-center">
          <h1 className={"text-black my-5"}>Search Movies</h1>
          <InputGroup className="mb-3">
            <InputGroup.Text className={"bg-light text-light-primary"}>
              <BiSearch size="2rem" />
            </InputGroup.Text>
            <FormControl
              placeholder="Search"
              value={searchMovie}
              onChange={(e) => setSearchMovie(e.target.value)}
              className={"bg-light text-black"}
            />
          </InputGroup>
        </Col>
        <SearchFilter
          value={searchMovie}
          data={movieData}
          renderResults={(results) => (
            <Row className="container">
              {results.map((item, i) => (
                <ProductCard data={item} key={i} />
              ))}
            </Row>
          )}
        />
      </Row>
    </Container>
  );
};

export default Home;
