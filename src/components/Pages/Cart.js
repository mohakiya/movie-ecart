import React, { useState, useRef } from "react";
import { Button, Container, Col, Row, Table, Modal } from "react-bootstrap";
import { useCart } from "react-use-cart";
import { BsCartX, BsCartDash } from "react-icons/bs";
import "../css/style.css";

const Cart = () => {
  let img_path = "https://image.tmdb.org/t/p/w500"; //image address to get the right image
  const Ref = useRef(null); //create a refrence
  const [timer, setTimer] = useState("00:00:00"); //state for timer
  const [show, setShow] = useState(false); //state for popup box
  const [disval, setDisval] = useState(); //state for calculating discount
  const {
    isEmpty,
    items,
    updateItemQuantity,
    removeItem,
    emptyCart,
    totalItems,
  } = useCart(); //cart data

  //popup box close function
  const handleClose = () => setShow(false);

  //order function
  const handleOrder = () => {
    setShow(true); //show popup box

    //calculating discount start
    if (totalItems >= 5) {
      const total = totalItems * 3;
      const dis = total * 0.2;
      setDisval(total - dis);
    } else if (totalItems >= 3) {
      const total = totalItems * 3;
      const dis = total * 0.1;
      setDisval(total - dis);
    }
    //calculating discount end

    clearTimer(getDeadTime()); //close popbox after 1min timer is complete
  };

  const clearTimer = (e) => {
    setTimer("00:00:60"); //set sec60 timer

    // If you try to remove this line the
    // updating of timer Variable will be
    // after 1000ms or 1sec
    if (Ref.current) clearInterval(Ref.current);
    const id = setInterval(() => {
      startTimer(e);
    }, 1000);
    Ref.current = id;
  };

  //deadline function
  const getDeadTime = () => {
    let deadline = new Date();

    // This is where you need to adjust if
    // you entend to add more time
    deadline.setSeconds(deadline.getSeconds() + 60);
    return deadline;
  };

  //start timer function
  const startTimer = (e) => {
    let { total, hours, minutes, seconds } = getTimeRemaining(e);
    if (total >= 0) {
      setTimer(
        (hours > 24 ? hours : "0" + hours) +
          ":" +
          (minutes > 60 ? minutes : "0" + minutes) +
          ":" +
          (seconds > 60 ? seconds : seconds)
      );
    }
    if (seconds === 0) {
      setShow(false);
    }
  };

  //remain time function
  const getTimeRemaining = (e) => {
    const total = Date.parse(e) - Date.parse(new Date());
    const seconds = Math.floor((total / 1000) % 60);
    const minutes = Math.floor((total / 1000 / 60) % 60);
    const hours = Math.floor((total / 1000 / 60 / 60) % 24);
    return {
      total,
      hours,
      minutes,
      seconds,
    };
  };

  return (
    <Container className="py-4 mt-5">
      <h1 className={`text-light-primary my-5 text-center`}>
        {isEmpty ? "Your Cart is Empty" : ""}
      </h1>
      <Row className="justify-content-center">
        <Table
          responsive="sm"
          striped
          bordered
          hover
          variant={"light"}
          className="mb-5"
        >
          <tbody>
            {items.map((item, index) => {
              return (
                <tr key={index}>
                  <td>
                    <div
                      style={{
                        background: "white",
                        height: "8rem",
                        overflow: "hidden",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <div style={{ padding: ".5rem" }}>
                        <img
                          src={img_path + item.poster_path}
                          style={{ width: "4rem" }}
                          alt={item.title}
                        />
                      </div>
                    </div>
                  </td>
                  <td>
                    <h6
                      style={{
                        whiteSpace: "nowrap",
                        width: "14rem",
                        overflow: "hidden",
                        textOverFlow: "ellipsis",
                      }}
                    >
                      {item.title}
                    </h6>
                  </td>
                  <td>
                    <Button
                      onClick={() =>
                        updateItemQuantity(item.id, item.quantity - 1)
                      }
                      className="ms-2"
                      style={{
                        borderRadius: "200px",
                        backgroundColor: "#22205d",
                      }}
                    >
                      -
                    </Button>
                    <span style={{ padding: "15px" }}>{item.quantity}</span>

                    <Button
                      onClick={() =>
                        updateItemQuantity(item.id, item.quantity + 1)
                      }
                      className="ms-2"
                      style={{
                        borderRadius: "200px",
                        backgroundColor: "#22205d",
                      }}
                    >
                      +
                    </Button>
                  </td>
                  <td>
                    <BsCartDash
                      className="cartdash"
                      size="1.8rem"
                      onClick={() => removeItem(item.id)}
                    />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
        {!isEmpty && (
          <Row
            style={{ position: "fixed", bottom: 0 }}
            className={`bg-light text-balck justify-content-center w-100`}
          >
            <Col className="py-2"></Col>
            <Col className="p-0" md={4}>
              <Button
                variant="danger"
                className="m-2"
                onClick={() => emptyCart()}
              >
                <BsCartX size="1.7rem" />
                Clear Shopping Cart
              </Button>

              <Button
                style={{ backgroundColor: "#22205d" }}
                onClick={handleOrder}
              >
                Order Now
              </Button>

              <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                  <Modal.Title>Order Details : </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <div class="promocontainer">
                    <p>
                      <sapn style={{ fontSize: "20px", fontWeight: "bold" }}>
                        $3 per movie:{" "}
                      </sapn>
                      <span class="promo">
                        Buy 3 Get 10% &nbsp;&nbsp;&nbsp;&nbsp; Buy 3, get 10%
                      </span>
                    </p>
                  </div>
                  <div>
                    You was bought {totalItems} movies. So you must pay $
                    {disval} now.
                  </div>

                  <h6>
                    Countdown:&nbsp;&nbsp;&nbsp;
                    {timer}
                  </h6>
                </Modal.Body>
                <Modal.Footer>
                  <Button
                    style={{ backgroundColor: "#22205d" }}
                    onClick={handleClose}
                  >
                    Cancel
                  </Button>
                </Modal.Footer>
              </Modal>
            </Col>
          </Row>
        )}
      </Row>
    </Container>
  );
};

export default Cart;
