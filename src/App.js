import React from "react";
import Header from "./components/Header";
import { Router } from "@reach/router";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
//Two pages : Home page and Cart page
import Home from "./components/Pages/Home";
import Cart from "./components/Pages/Cart";

function App() {
  return (
    <main style={{ height: "100vh", overflowY: "auto" }}>
      <Header />
      <Router>
        <Home path="/" />
        <Cart path="/cart" />
      </Router>
    </main>
  );
}

export default App;
